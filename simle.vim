" Vim syntax file
" Language:    Simle
" Maintainer:  Forat Seif <foratseif@gmail.com>
" URL:    	   https://github.com/foratseif/vim-simle
" ----------------------------------------------------------------------------
" Thanks to the authors of:
"   ruby.vim
"   python.vim
"   java.vim
" ----------------------------------------------------------------------------

if exists("b:current_syntax")
    "finish
endif

"" Comments and Documentation
"syn match   simleSharpBang     "\%^#!.*" display
syn keyword simleTodo	       FIXME NOTE TODO OPTIMIZE XXX todo contained
syn match   simleCommentLine   "#.*" contains=simleTodo,simleCommentBlock " add simleSharpBang!!
syn region  simleCommentBlock  start="#\*"  end="\*#" contains=simleTodo

"" special words
syn keyword simleRepeat	       while
syn keyword simleException     try catch
syn keyword simleConditional   elif else if

"" colored sequences
syn match   simleClass         "\<class\>"                nextgroup=simleClassDec skipwhite
syn match   simleClassDec      "\(:[A-Za-z0-9_]\+\s*\)\{1,2}" contained nextgroup=simleClassClib " contains=simleConstant,simleOperator
syn match   simleClassClib     "\(\<_clib\s*=\s*\"\S*\"\)\{0,1}" contained contains=simleString

"" string and string escape matching (copied from python, 0 understanding of it
syn region  simleString
      \ start=+[uU]\=\z(['"]\)+ end="\z1" skip="\\\\\|\\\z1"
      \ contains=simleEscape
syn region  simleString
      \ start=+[uU]\=\z('''\|"""\)+ end="\z1" keepend
      \ contains=simleEscape

syn match   simleEscape	+\\[abfnrtv'"\\]+ contained
syn match   simleEscape	"\\\o\{1,3}" contained
syn match   simleEscape	"\\x\x\{2}" contained
syn match   simleEscape	"\%(\\u\x\{4}\|\\U\x\{8}\)" contained
" Python allows case-insensitive Unicode IDs: http://www.unicode.org/charts/
syn match   simleEscape	"\\N{\a\+\%(\s\a\+\)*}" contained
syn match   simleEscape	"\\$"

"syn keyword pythonStatement	False, None, True
"syn keyword pythonStatement	as assert break continue del exec global
"syn keyword pythonStatement	lambda nonlocal pass print return with yield
"syn keyword pythonStatement	class def nextgroup=pythonFunction skipwhite
"syn keyword pythonConditional	elif else if
"syn keyword pythonInclude	from import include


" link shit to colors
hi def link simleCommentLine   Comment
hi def link simleCommentBlock  Comment

hi def link simleRepeat	       Repeat
hi def link simleException     Exception
hi def link simleConditional   Conditional

hi def link simleClass	       Define
hi def link simleClassDec      Statement
hi def link simleClassClib     Include

hi def link simleString	       String
hi def link simleEscape        SpecialChar

"hi def link pythonInclude      Include
"hi def link pythonStatement    Statement
"hi def link pythonFunction     Function

let b:current_syntax = "simle"

