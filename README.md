# Simle syntax file for vim

Creating vim syntax highlighting for [simle-programing-language](https://www.github.com/foratseif/simle-programming-language.git "Simle Programming Language Repository").

A lot of the work is based on: ```ruby.vim```, ```python.vim``` and ```c.vim```.

# Progress:
* ```comment line``` and ```comment block```
    * ![alt text](README/comment.gif?raw=true "comment")

* ```class``` definition
    * ```class``` and ```super class``` name highlight
    * ```clib``` in class definition highlighting
    * ![alt text](README/class_def.gif?raw=true "class_def")

* ```string``` and ```escape sequences``` (copied directly from python)
    * ![alt text](README/string.gif?raw=true "string")
